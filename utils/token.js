import { Config } from 'config.js';
import { Utils } from 'utils.js';

class Token {
  constructor() {
    this.verifyUrl = Config.restUrl + 'token/verify';
    this.tokenUrl = Config.restUrl + 'token/user';
  }

  verify() {
    var token = wx.getStorageSync('token');
    if (!token) {
      this.getTokenFromServer();
    }
    else {
      this._veirfyFromServer(token);
    }
    
  }

  _veirfyFromServer(token) {
    var that = this;
    var utils = new Utils();
    var header = utils.makeHeader();
    wx.request({
      url: that.verifyUrl,
      method: 'POST',
      data: {
        token: token
      },
      header: header,
      success: function (res) {
        var valid = res.data.isValid;
        if (!valid) {
          that.getTokenFromServer();
        }
      }
    })
  }

  getTokenFromServer(callBack) {
    var that = this;
    var utils = new Utils();
    var header = utils.makeHeader();
    wx.login({
      success: function (res) {
        wx.request({
          url: that.tokenUrl,
          method: 'POST',
          data: {
            code: res.code
          },
          header: header,
          success: function (res) {
            wx.setStorageSync('token', res.data.token);
            console.log('服务器的token');
            callBack && callBack(res.data.token);
          }
        })
      }
    })
  }
}

export { Token };