import { Config } from 'config.js';
import { Token } from 'token.js';
import { Utils } from 'utils.js';

class Base{
  constructor(){
    this.baseRestUrl = Config.restUrl;
    this.onPay = Config.onPay;
  }
  //http 请求类, 当noRefech为true时，不做未授权重试机制
  request(params, noRefetch) {
    var that = this;
    var url = this.baseRestUrl + params.url;
    if (!params.type) {
      params.type = 'GET';
    }
    var utils = new Utils();
    var header = utils.makeHeader();
    wx.request({
      url: url,
      data: params.data,
      method: params.type,
      header: header,
      success: function (res) {
        // 判断以2（2xx)开头的状态码为正确
        // 异常不要返回到回调中，就在request中处理，showToast一个统一的错误即可
        var code = res.statusCode.toString();
        var startChar = code.charAt(0);
        if (startChar == '2') {
          params.sCallback && params.sCallback(res.data);
        } else {
          //token失效再次获取
          if (code == '401') {
            if (!noRefetch) {
              that._refetch(params);
            }
          }
          
          that._processError(res);
          params.eCallback && params.eCallback(res.data);
          
        }
      },
      fail: function (err) {
        that._processError(err);
      }
    });
  }

  _processError(err) {
    console.log(err);
  }

  _refetch(param) {
    var token = new Token();
    token.getTokenFromServer((token) => {
      this.request(param, true);
    });
  }
  
  /*获得元素上的绑定的值*/
  getDataSet(event, key) {
    return event.currentTarget.dataset[key];
  };
};

export { Base };