import { Token } from 'utils/token.js';

import md5 from 'utils/md5.js';

//app.js
App({
  // 接口地址
  
  onLaunch: function () {


    console.log('app onLaunch');
    var tokenModel = new Token();
    tokenModel.verify();
    var self = this;

    var token = wx.getStorageSync('token');//用户的缓存信息
    if (token){
      console.log('token已经缓存' + token)
    }




  },

  //登录，获取token
  getToken: function () {
    let _this = this;
    return new Promise(function (resolve, reject) {
      let token = wx.getStorageSync('token'); 
      if (token){
        resolve(token);
        //reject('1111');
      }else{
        wx.login({
          success: res => {
            // 发送 res.code 到后台换取 openId, sessionKey, unionId 
            if (res.code) {
              wx.request({
                url: "http://www.wx_thinkphp.com/index.php/api/v1/token/user",
                method: 'POST',
                data: {
                  code: res.code
                },
                success: res => {
                  wx.setStorageSync('token', res.data.token);
                  console.log('已经获取服务器的token');
                  //储存返回的token 
                  resolve(res);
                }
              })
            }
            else {
              console.log('获取用户登录态失败！' + res.data);
              reject('error');
            }
          }
        });
      }
      
    });
  },


  globalData: {
    userInfo: {
       'phone': ''
    },
	  header: {
      'content-type': 'application/x-www-form-urlencoded',//'application/json',
      'Token': '7706c7e5ea27b2041a1e93b93d7f225f',//通讯密钥
    },
    appSecret: '123456'
  }
})