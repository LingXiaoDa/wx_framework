/**
 * 小程序配置文件
 */

var host = "http://www.wx_thinkphp.com"

var config = {

    host,

    // 登录地址，用于建立会话
    loginUrl: `${host}/api/v1/getToken`,
    
    // 上传文件接口
    uploadFileUrl: `${host}/upload`,

};
export { config };
//module.exports = config;