import { Config } from 'config.js';
import md5 from 'md5.js';

class Utils {
  constructor() {

  }
  getTime() {
    //var date = new Date();
    return Math.round(Date.now() / 1000);//时间戳
  }

  createNonceStr() {
    return Math.random().toString(36).substr(2, 15);//生成随机串
  }
  /** 
  *  数组对象按key升序, 并生成 md5_hex 签名
    * @param {Array/Object} obj   数组对象
    * @return {String}  encrypted md5加密后的字符串
    */
  makeSign(obj) {
    if (!obj) { 
      console.log('需要加密的数组对象为空') 
      return;
    }
    var str = '';
    //生成key升序数组
    var arr = Object.keys(obj);
    arr.sort();
    for (var i in arr) {
      str += arr[i] + obj[arr[i]];
    }
    var encrypted = md5(str);
    return str;
  }
  /**
   * 生成头部必须携带的参数
   */
  makeHeader(){
    var timestamp = this.getTime();//时间戳
    var nonceStr = this.createNonceStr();//随机串
    //根据appSecret生成签名
    var sign = md5(nonceStr + timestamp + Config.appSecret);
    var header = {
      'content-type': 'application/x-www-form-urlencoded',
      'token': wx.getStorageSync('token'),
      'app-id': Config.appId,
      'app-sign': sign,
      'timestamp': timestamp,
      'nonce-str': nonceStr,
      };
    return header;
  }
}

export { Utils };