// pages/home/home.js
import { Home } from 'homeModel.js';
var home = new Home();
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loadingHidden: false
  },
 

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    app.getToken().then(res=> {
      console.log(res);
      //that.getData();  
    }).catch(e => {
        console.log(e)
      })
    // app.getToken().then(function (e) {
    //   // success
    //   console.log(e)
    // }, function (e) {
    //   // failure
    //   console.log('222')
    // });


  },
  getData: function () {
    var that = this;
    var id = 1;
    var data = home.getBannerData(id, (res)=>{
      console.log(res.items);
      that.setData({
        bannerArr: res.items,
      });
    });


    /*获取主题信息*/
    // home.getThemeData((data) => {
    //   that.setData({
    //     themeArr: data,
    //     loadingHidden: true
    //   });
    // });
    
  },
  
  /*跳转到商品详情*/
  onProductsItemTap: function (event) {
    var id = home.getDataSet(event, 'id');
    wx.navigateTo({
      url: '../product/product?id=' + id
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})